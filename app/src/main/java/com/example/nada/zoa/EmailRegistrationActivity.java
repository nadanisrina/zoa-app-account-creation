package com.example.nada.zoa;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;

public class EmailRegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private Button registerBtn;
    private TextInputLayout firstNameLayout, lastNameLayout, emailLayout, passwordLayout, passwordConfirmationLayout, usernameLayout;

    private static boolean isFormError = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_registration);

        registerBtn = (Button) findViewById(R.id.register_btn);
        registerBtn.setOnClickListener(this);

        firstNameLayout = (TextInputLayout) findViewById(R.id.first_name_layout);
        lastNameLayout = (TextInputLayout) findViewById(R.id.last_name_layout);
        emailLayout = (TextInputLayout) findViewById(R.id.email_layout);
        usernameLayout = (TextInputLayout) findViewById(R.id.username_layout);
        passwordLayout = (TextInputLayout) findViewById(R.id.password_layout);
        passwordConfirmationLayout = (TextInputLayout) findViewById(R.id.password_confirmation_layout);

        firstNameLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    isEmpty(firstNameLayout);
                }
            }
        });

        lastNameLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    isEmpty(firstNameLayout);
                    isEmpty(lastNameLayout);
                }
            }
        });

        emailLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    isEmpty(firstNameLayout);
                    isEmpty(lastNameLayout);
                    isEmpty(emailLayout);
                }
            }
        });

        passwordLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    isEmpty(firstNameLayout);
                    isEmpty(lastNameLayout);
                    isEmpty(emailLayout);
                    isEmpty(passwordLayout);
                }
            }
        });

        passwordLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("##$ Text Count", "onTextChanged: " + passwordLayout.getEditText().length());
                if (passwordLayout.getEditText().length() < 6)  {
                    passwordLayout.setErrorEnabled(true);
                    passwordLayout.setError("Password length min 6 characters");
                    Log.d("##$ Text Count", "onTextChanged: kurang " + passwordLayout.getEditText().length());
                } else {
                    passwordLayout.setErrorEnabled(false);
                    Log.d("##$ Text Count", "onTextChanged: lebih " + passwordLayout.getEditText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        passwordConfirmationLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    isEmpty(firstNameLayout);
                    isEmpty(lastNameLayout);
                    isEmpty(emailLayout);
                    isEmpty(passwordLayout);
                    isEmpty(passwordConfirmationLayout);
                    isMatch(passwordConfirmationLayout, passwordLayout);
                }
            }
        });
    }

    private static void isEmpty(TextInputLayout inputLayout) {
        if (inputLayout.getEditText().getText().toString().trim().length() == 0) {
            inputLayout.setErrorEnabled(true);
            inputLayout.setError("Please enter your " + inputLayout.getHint());
            isFormError = true;
        } else {
            inputLayout.setErrorEnabled(false);
            isFormError = false;
        }
    }

    private void isMatch(TextInputLayout inputLayout, TextInputLayout inputLayout2) {
        String a = (String) inputLayout.getEditText().getText().toString();
        String b = (String) inputLayout2.getEditText().getText().toString();
        Log.d("Input1: ", a);
        Log.d("Input2: ", b);
        if (!a.equals(b) && a != null) {
            inputLayout.setErrorEnabled(true);
            inputLayout.setError("Password not match");
            isFormError = true;
        } else {
            inputLayout.setErrorEnabled(false);
            isFormError = false;
        }
    }

    private static void validateEditText(TextInputLayout textInputLayout, String... rules) {
        /* TODO Write Array based rule match validation */
        for (String rule: rules) {
            switch (rule) {
                case "required":
                    isEmpty(textInputLayout);
                    break;
            }
        }
    }

    private static int getIdAssignedByR(Context pContext, String pIdString)
    {
        // Get the Context's Resources and Package Name
        Resources resources = pContext.getResources();
        String packageName  = pContext.getPackageName();

        // Determine the result and return it
        int result = resources.getIdentifier(pIdString, "id", packageName);
        return result;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register_btn:
                Log.d("###AA", "onClick: Register Clicked!");

                validateEditText(this.firstNameLayout, "required");
                validateEditText(this.lastNameLayout, "required");
                validateEditText(this.emailLayout, "required");
                validateEditText(this.passwordLayout, "required");
                isMatch(this.passwordConfirmationLayout, passwordLayout);
                validateEditText(this.passwordConfirmationLayout, "required");


                if (!isFormError) {
                    final ProgressDialog dialog = new ProgressDialog(EmailRegistrationActivity.this);
                    dialog.setMessage("Registering new account ...");
                    dialog.show();


                    final String url = "http://192.168.43.171/beta.zoa.id/api/web/account/register";
                    RequestQueue requestQueue = Volley.newRequestQueue(this);

                    String deviceToken = FirebaseInstanceId.getInstance().getToken();

                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("first_name",getTextInputContent(this.firstNameLayout));
                    params.put("last_name",getTextInputContent(this.lastNameLayout));
                    params.put("email",getTextInputContent(this.emailLayout));
                    params.put("username",getTextInputContent(this.usernameLayout));
                    params.put("password",getTextInputContent(this.passwordLayout));
                    params.put("device_token", deviceToken);

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, new JSONObject(params),
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("###", "onResponse: " + response);
                                    dialog.dismiss();
                                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    finish();
                                    startActivity(intent);
                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    VolleyLog.d(error.getMessage());
                                    dialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Registration failed", Toast.LENGTH_SHORT).show();
                                }
                            });

                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                            30000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    requestQueue.add(jsonObjectRequest);
                }

                break;
        }
    }

    public static String getTextInputContent(TextInputLayout input)
    {
        return input.getEditText().getText().toString();
    }
}